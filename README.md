# LoRaWAN LOW POWER WEATHER STATION


Knowledge about climate behavior and its prediction is vital in order to prevent ecological, economic and social
damage. For this reason, this is an issue that is responsible for government, trade, agriculture, and other range
of entities that are interested in knowing how climate can affect them.
In this project, we will make a weather station that measures air pressure, altitude, temperature, total rainfall
and air humidity using [ULP Lora 2.1 board](https://gitlab.com/icfoss/OpenIoT/ulplora). The interfacing sensors are used to collect the various data
mentioned above and the board's inbuilt rf module used to send the data to the nearest gateway and this data
is pushed into the influx Db, which saves the data for data acquisition! The weather station is working in low
power mode. So that it is powered by a single battery of 4.2 V.As we work through the project we will connect
up the various sensors. In this process, we create a software sketch that will run the weather station.

## Getting Started


- Make sure that you have a ULP LoRa Board.

- Install project library from [here](https://gitlab.com/arunchandra2500/weather_station_ulplora_board/-/tree/master/libraries) . Copy the library to ~/Arduino/libraries/

- Select board : Arduino Pro Mini 3.3v 8Mhz

## Prerequisites

Arduino IDE - 1.8.9 [Tested]

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/arunchandra2500/weather_station_ulplora_board/-/blob/master/LICENCE.md) file for details

## Acknowledgments


- LMIC by [matthijskooijman](https://github.com/matthijskooijman/arduino-lmic)
